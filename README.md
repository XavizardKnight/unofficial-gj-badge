# UNOFFICIAL "AVAILABLE ON GAME JOLT" BADGE
<img alt="Stars" src="https://img.shields.io/badge/dynamic/json.svg?style=for-the-badge&label=Stars&url=https://gitlab.com/api/v4/projects/27445856&query=star_count&logo=star&color=orange" href="https://gitlab.com/XavizardKnight/unofficial-gj-badge">
<br>
<center>
<img src="badge/prebaked/black.png" alt="Game Jolt black badge" width="250"/>
</center>
<br>

This repository contains the vector file of the unofficial "Available on Game Jolt" badge.

This badge mimics the style of the Itchio badge, but with Game Jolt's icon and logos, useful for telling your people that your game is available on Game Jolt. I've made several versions of the badge, with black, grey, white, green, dark green, blue and pink backgrounds and the logo and icons have both light and dark versions.

You can use this badges file for whatever you like; credit is not required.
<br><br>
Here's how the badge looks alongisde the Itchio and Google Play ones:

![A collection of badges](readme/badgeComparation.png)

And here is a collection of all of the versions of this Game Jolt badge:

![All the versions of the badge](readme/badgeGallery.png)

I've created this badge in a single SVG for all the versions. All the different backgrounds and color variations are in different layers, so you can show and hide the layers you want to create your custom version of the badge.

![APNG demo of the layers](readme/layerDemo.png)
<br><br>

## USAGE

Want to use it? First, clone this repository into your local machine.<br><br>
You can use the always effective terminal by using this commands:
```
git clone https://gitlab.com/XavizardKnight/unofficial-gj-badge.git
cd unofficial-gj-badge
```
You can also download it from the web interface. In the button bar (buttons that say "History", "Find file", etc.) search a button with a download icon and then select the archive format in which you want the repo to be downloaded (you might want .zip or .tar.gz).

![Download instructions](readme/downloadInstructions.png)

Once downloaded, use any archive extractor to extract the folders (if you don't have any, I recommend [PeaZip](https://peazip.github.io)).

Then navigate into `unofficial-gj-badge/badge/gj-badge.svg` to find the master svg file. Some prebaked samples are available in `unofficial-gj-badge/badge/prebaked/`.

I've used Inkscape for creating the vectorial file. It might work with other programs with SVG support, but no guarantees.
<br><br>

### Inside the vectorial file

In Inkscape, search for the layer panel (Ctrl+Maj+L). Once opened, you should see all the layers. Here you can click on the eye icon for toggle on and off each layer.

Once ready, you might want to export this into a PNG. From Inkscape, go to **File > Export to PNG**.
<br><br>

## FAQ
### **Seriously there's still no official Game Jolt badge?**
Nope, there's [this forum thread](https://gamejolt.com/f/available-on-gamejolt-badge/5314) of people requesting this from years ago and nothing happened. In this same thread, some users shared their own custom versions of the badge, but the official version is nowhere to be seen.

### **Where did you obtained these Game Jolt logos? Why did you use this colors?**
In [Game Jolt's about page](https://gamejolt.com/about). There are all of their icons and logos avalible for download and the RGB color codes for their official colors.

The grey comes from [Game Jolt official ribbons](https://github.com/gamejolt/ribbons); and I've also added pure white and pure black because why not.
